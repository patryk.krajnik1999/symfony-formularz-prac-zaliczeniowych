<?php

namespace App\Repository;

use App\Entity\SubmittedWork;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SubmittedWork>
 *
 * @method SubmittedWork|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubmittedWork|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubmittedWork[]    findAll()
 * @method SubmittedWork[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubmittedWorkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubmittedWork::class);
    }

//    /**
//     * @return SubmittedWork[] Returns an array of SubmittedWork objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SubmittedWork
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
