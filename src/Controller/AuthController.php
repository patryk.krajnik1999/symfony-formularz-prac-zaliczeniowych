<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    private UserRepository $userRepository;

    public function __construct(
        UserRepository $userRepository,
    ) {
        $this->userRepository = $userRepository;
    }

    #[Route('/register', name: 'app_auth')]
    public function register(
        UserPasswordHasherInterface $userPasswordHasher,
        ManagerRegistry $doctrine,
        Request $request
    ): JsonResponse {
        try {
            $entityManager = $doctrine->getManager();

            $content = json_decode($request->getContent(), true);

            $user = new User();
            $user->setEmail($content['email']);
            $password = $userPasswordHasher->hashPassword(
                $user,
                $content['password'],
            );
            $user->setPassword($password);
            $user->setRoles([$content['role']]);
            $entityManager->persist($user);

            $entityManager->flush();
        } catch (Exception $ex) {
            return $this->json($ex);
        }

        return $this->json([
            'email'    => $user->getEmail(),
            'password' => $user->getPassword(),
            'roles'     => $user->getRoles(),
            'userId'       => $user->getId(),
        ]);
    }

    #[Route('/login', name: 'app_login')]
    public function login(
        UserPasswordHasherInterface $userPasswordHasher,
        Request $request
    ): JsonResponse {
        try {
            $content = json_decode($request->getContent(), true);

            $user = $this->userRepository->findOneBy(
                ['email' => $content['email']]
            );
            if ($user == null) {
                $isValid = false;
            } else {
                $isValid = $userPasswordHasher->isPasswordValid(
                    $user,
                    $content['password'],
                );
            }
        } catch (Exception $ex) {
            return $this->json($ex);
        }

        return $this->json([
            'isValid' => $isValid,
            'userId' => $user?->getId(),
            'roles'     => $user?->getRoles(),
        ]);
    }
}
