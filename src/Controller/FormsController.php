<?php

namespace App\Controller;
use App\Entity\SubmittedWork;
use App\Entity\Work;
use App\Repository\UserRepository;
use App\Repository\WorkRepository;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FormsController extends AbstractController
{
    private WorkRepository $workRepository;
    private UserRepository $userRepository;

    public function __construct(
        WorkRepository $workRepository, UserRepository $userRepository
    ) {
        $this->workRepository = $workRepository;
        $this->userRepository = $userRepository;
    }


    public function GetSubmits($allSubmits): array
    {
        $submits = [];
        for ($j = 0; $j < count($allSubmits); $j++) {
            $submit    = $allSubmits[$j];
            $data       = [
                'userId' => $submit->getUser()->getId(),
                'userEmail' => $submit->getUser()->getEmail(),
                'filePath'    => $submit->getFilePath(),
                'dateTime'    => $submit->getDateTime(),
                'fileType'    => $submit->getFileType(),
            ];
            $submits[] = $data;
        }

        return $submits;
    }

    #[Route('/forms', name: 'app_forms')]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'dupa',
            'path'    => 'src/Controller/FormsController.php',
        ]);
    }

    #[Route('/works', name: 'app_works', methods: ['GET'])]
    public function works(): JsonResponse
    {
        try {
            $allWorks = $this->workRepository->findAll();
            $works    = [];
            for ($i = 0; $i < count($allWorks); $i++) {
                $work    = $allWorks[$i];
                $data    = [
                    'id'          => $work->getId(),
                    'topic'       => $work->getTopic(),
                    'description' => $work->getDescription(),
                    'openDate'    => $work->getOpenDate(),
                    'closeDate'   => $work->getCloseDate(),
                    'userId'      => $work->getUser()->getId(),
                    'submits'     => $this->GetSubmits($work->getSubmittedWorks()),
                ];
                $works[] = $data;
            }
        } catch (Exception $ex) {
            return $this->json($ex);
        }

        return $this->json($works);
    }

    #[Route('/addWork', name: 'app_addWork', methods: ['POST'])]
    public function newWork(
        ManagerRegistry $doctrine,
        Request $request
    ): JsonResponse {
        try {
            $entityManager = $doctrine->getManager();

            $content = json_decode($request->getContent(), true);

            $openDate = new \DateTime($content['openDate']);
            $closeDate = new \DateTime($content['closeDate']);

            $user = $this->userRepository->findOneBy(['id' => $content['userUuId']]);

            $work = new Work();
            $work->setTopic($content['topic']);
            $work->setDescription($content['description']);
            $work->setOpenDate($openDate);
            $work->setCloseDate($closeDate);
            $work->setUser($user);
            $entityManager->persist($work);

            $entityManager->flush();
        } catch (Exception $ex) {
            return $this->json($ex);
        }

        return $this->json($content);
    }

    #[Route('/uploadFile', name: 'app_uploadFile', methods: ['POST'])]
    public function uploadFile(
        ManagerRegistry $doctrine,
        Request $request
    ): Response {
        try {
            $entityManager = $doctrine->getManager();

            $content = json_decode($request->getContent(), true);

            $uploaded = $content["file"];

            [$type, $data] = explode(';', $uploaded);

            $mimeType = substr($type, 5); // remove `data:` prefix
            [, $ext] = explode('/', $mimeType);
            [, $data] = explode(',', $data); // remove `base64,` prefix

            $file = base64_decode($data);

            $filePath = tempnam(sys_get_temp_dir(), 'uploaded-image');

            file_put_contents($filePath, $file);

            $uploadedFile = new File($filePath);
            $destinationName = microtime(true).'.'.$ext;

            $uploadedFile->move(__DIR__ . '/../../public/uploads', $destinationName);

            $work = $this->workRepository->findOneBy(["id" => $content["workId"]]);
            $user = $this->userRepository->findOneBy(["id" => $content["userId"]]);
            $dateTime = new \DateTime($content["dateTime"]);

            $submittedWork = new SubmittedWork();
            $submittedWork->setFilePath('/uploads/'.$destinationName);
            $submittedWork->setWork($work);
            $submittedWork->setUser($user);
            $submittedWork->setDateTime($dateTime);
            $submittedWork->setFileType($mimeType);
            $entityManager->persist($submittedWork);

            $entityManager->flush();
        } catch (Exception $ex) {
            return $this->$ex;
        }

        return new JsonResponse(['path' => '/uploads/'.$destinationName]);
    }
}
