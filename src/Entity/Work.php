<?php

namespace App\Entity;

use App\Repository\WorkRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WorkRepository::class)]
class Work
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $topic = null;

    #[ORM\Column(length: 255)]
    private ?string $description = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $openDate = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $closeDate = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\OneToMany(mappedBy: 'work', targetEntity: SubmittedWork::class, orphanRemoval: true)]
    private Collection $submittedWorks;

    public function __construct()
    {
        $this->submittedWorks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTopic(): ?string
    {
        return $this->topic;
    }

    public function setTopic(string $topic): static
    {
        $this->topic = $topic;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getOpenDate(): ?\DateTimeInterface
    {
        return $this->openDate;
    }

    public function setOpenDate(\DateTimeInterface $openDate): static
    {
        $this->openDate = $openDate;

        return $this;
    }

    public function getCloseDate(): ?\DateTimeInterface
    {
        return $this->closeDate;
    }

    public function setCloseDate(\DateTimeInterface $closeDate): static
    {
        $this->closeDate = $closeDate;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, SubmittedWork>
     */
    public function getSubmittedWorks(): Collection
    {
        return $this->submittedWorks;
    }

    public function addSubmittedWork(SubmittedWork $submittedWork): static
    {
        if (!$this->submittedWorks->contains($submittedWork)) {
            $this->submittedWorks->add($submittedWork);
            $submittedWork->setWork($this);
        }

        return $this;
    }

    public function removeSubmittedWork(SubmittedWork $submittedWork): static
    {
        if ($this->submittedWorks->removeElement($submittedWork)) {
            // set the owning side to null (unless already changed)
            if ($submittedWork->getWork() === $this) {
                $submittedWork->setWork(null);
            }
        }

        return $this;
    }
}
