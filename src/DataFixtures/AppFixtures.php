<?php

namespace App\DataFixtures;

use App\Entity\Work;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $work = new Work();
        $openDate = new \DateTime('20-10-2023');
        $closeDate = new \DateTime('27-10-2023');
        $work->setTopic('Temat 1');
        $work->setDescription('Jakiś opis jakieś pracy zaliczeniowej.');
        $work->setOpenDate($openDate);
        $work->setCloseDate($closeDate);
        $manager->persist($work);

        $work2 = new Work();
        $openDate = new \DateTime('22-10-2023');
        $closeDate = new \DateTime('29-10-2023');
        $work2->setTopic('Temat 2');
        $work2->setDescription('Jakiś opis jakieś pracy zaliczeniowej.');
        $work2->setOpenDate($openDate);
        $work2->setCloseDate($closeDate);
        $manager->persist($work2);

        $work3 = new Work();
        $openDate = new \DateTime('21-10-2023');
        $closeDate = new \DateTime('28-10-2023');
        $work3->setTopic('Temat 3');
        $work3->setDescription('Jakiś opis jakieś pracy zaliczeniowej.');
        $work3->setOpenDate($openDate);
        $work3->setCloseDate($closeDate);
        $manager->persist($work3);

        $manager->flush();
    }
}
